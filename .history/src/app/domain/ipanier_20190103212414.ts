export interface IPanier {
  panier_id: number;
  produit
  date: Date;
  quantite: number;
}
