export interface IPanier {
  panier: number;
  date: Date;
  quantite: number;
}
