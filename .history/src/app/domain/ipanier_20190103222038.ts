import { IProduit } from './iProduit';

export interface IPanier {
  panier_id?: number;
  produit_id: IProduit[];
  dateP: Date;
  quantite: number;
}
