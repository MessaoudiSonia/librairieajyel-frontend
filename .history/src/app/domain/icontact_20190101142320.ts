
export interface ICategorie {
  id?: number;
  libelle_categorie: string;
  produit: IProduit;
}
