
export interface IContact {
  id?: number;
  nom: string;
  mail: string;
  tel: number;
  message: string;
}
