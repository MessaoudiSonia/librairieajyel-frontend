export interface IProduit {
  reference_Produit: string;
  libelle: string;
  prix: number;
  qte: number;
  qte_seuil: number ;
  description: string;
  image: string;
}
