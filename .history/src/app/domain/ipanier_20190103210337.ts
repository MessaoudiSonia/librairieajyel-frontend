export interface IPanier {
  panierPK: number;
  date: Date;
  quantite: int;
}
