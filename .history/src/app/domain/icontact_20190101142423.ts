
export interface ICategorie {
  id?: number;
  nom: string;
  mail: string;
  tel: number;
  message: string
}
