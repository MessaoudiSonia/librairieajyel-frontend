export interface IPanier {
  panier_id: number;
  produit_id:IProduit[];
  date: Date;
  quantite: number;
}
