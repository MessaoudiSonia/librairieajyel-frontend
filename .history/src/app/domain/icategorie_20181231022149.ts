import { ProduitAjoutComponent } from "../components/produit-ajout/produit-ajout.component";

export interface ICategorie {
  id_categorie?: number;
  libelle_categorie: string;
  produit:ProduitAjoutComponent;
}
