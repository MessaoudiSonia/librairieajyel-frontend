export interface IPanier {
  panier_id: number;
  date: Date;
  quantite: number;
}
