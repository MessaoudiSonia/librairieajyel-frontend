
export interface ICategorie {
  id?: number;
  nom: string;
  produit: IProduit;
}
