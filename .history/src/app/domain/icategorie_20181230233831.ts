export interface Icategorie {
  id_categorie?: number;
  cin: number;
  pseudo: string;
  mdp: string;
  prenom: string;
  nom: string;
  email: string;
  tel: number;
  adresse: String;
}
