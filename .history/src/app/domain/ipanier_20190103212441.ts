export interface IPanier {
  panier_id: number;
  produit_id:IProduit.i
  date: Date;
  quantite: number;
}
