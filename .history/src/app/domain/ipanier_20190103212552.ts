import { IProduit } from "./iProduit";

export interface IPanier {
  panier_id?: number;
  produit_id:IProduitoduit[];
  date: Date;
  quantite: number;
}
