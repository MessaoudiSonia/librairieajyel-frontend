import { IProduit } from './iProduit';

export interface IPanier {
  panier_id?: number;
  produit_id: IProduit[];
  date: string;
  quantite: number;
}
