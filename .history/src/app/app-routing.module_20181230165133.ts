import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcceuilComponent } from './components/acceuil/acceuil.component';
import { ProduitListComponent } from './components/produit-list/produit-list.component';
import { PanierListComponent } from './components/panier-list/panier-list.component';
import { ProduitAjoutComponent } from './components/produit-ajout/produit-ajout.component';
import { FactureListComponent } from './components/facture-list/facture-list.component';
import { ClientListComponent } from './components/client-list/client-list.component';
import { ClientAjoutComponent } from './components/client-ajout/client-ajout.component';
import { DetailProduitsComponent } from './components/detail-produits/detail-produits.component';
import { SidebarComponent} from './components/sidebar/sidebar.component';
import {NotfoundComponent} from './components/notfound/notfound.component';
import { FacteurClientComponent} from './components/facteur-client/facteur-client.component';
import {} from './components/categorie'


const routes: Routes = [
  { path: 'listeproduit', component: ProduitListComponent },
  { path: '', component: AcceuilComponent },
  { path: 'listeclient', component: ClientListComponent },
  { path: 'listepanier', component: PanierListComponent },
  { path: 'ajoutproduit', component: ProduitAjoutComponent },
  { path: 'ajoutclient', component: ClientAjoutComponent },
  { path: 'listefacture', component: FactureListComponent },
  { path: 'Sidebar', component: SidebarComponent},
  { path: 'detailproduits/:reference_Produit', component: DetailProduitsComponent },
  { path: '**', component: NotfoundComponent },
  { path: 'factureClient', component: FacteurClientComponent}
  { path: 'categorie', component: FacteurClientComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


