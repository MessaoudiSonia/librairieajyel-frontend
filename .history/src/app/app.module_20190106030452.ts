import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ClientListComponent } from './components/client-list/client-list.component';
import { AcceuilComponent } from './components/acceuil/acceuil.component';
import { ProduitListComponent } from './components/produit-list/produit-list.component';
import { PanierListComponent } from './components/panier/panier-list.component';
import { ProduitAjoutComponent } from './components/produit-ajout/produit-ajout.component';
import { ClientAjoutComponent } from './components/client-ajout/client-ajout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {CommonModule} from '@angular/common';
import { FilterPipe } from 'ngx-filter-pipe';
import { MypipePipe } from './mypipe.pipe';
import { DetailProduitsComponent } from './components/detail-produits/detail-produits.component';
import { SidebarComponent} from './components/sidebar/sidebar.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { FacteurClientComponent } from './components/facteur-client/facteur-client.component';
import { NgxPaginationModule} from 'ngx-pagination';
import { PartenaireComponent } from './components/partenaire/partenaire.component';
import { CategorieComponent } from './components/categorie/categorie.component';
import { ContactezNousComponent } from './components/contactez-nous/contactez-nous.component';
import { ContactnotificationComponent } from './components/contactnotification/contactnotification.component';
import {AgmCoreModule} from '@agm/core';


import { LoginComponent } from './components/login/login.component';
import { UserComponent } from './components/user/user.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { AdminComponent } from './components/admin/admin.component';
import { PmComponent } from './components/pm/pm.component';
import { httpInterceptorProviders } from './security/auth/auth-interceptor';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ClientListComponent,
    AcceuilComponent,
    ProduitListComponent,
    PanierListComponent,
    ProduitAjoutComponent,
    ClientAjoutComponent,
    FilterPipe,
    MypipePipe,
    DetailProduitsComponent,
    SidebarComponent,
    NotfoundComponent,
    FacteurClientComponent,
    PartenaireComponent,
    CategorieComponent,
    ContactezNousComponent,
    ContactnotificationComponent,


// tslint:disable-next-line:indent
	  LoginComponent,
    UserComponent,
    RegisterComponent,
    HomeComponent,
    AdminComponent,
    PmComponent
     ],
  imports: [
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    HttpClientModule,
    AppRoutingModule,
    AngularFontAwesomeModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    NgxPaginationModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCz3ubFoA8h-NFiRx3wUqd40nF477_i3gI' })
    ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
