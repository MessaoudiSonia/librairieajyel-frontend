import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcceuilComponent } from './components/acceuil/acceuil.component';
import { ProduitListComponent } from './components/produit-list/produit-list.component';
import { PanierListComponent } from './components/panier/panier-list.component';
import { ProduitAjoutComponent } from './components/produit-ajout/produit-ajout.component';
import { ClientListComponent } from './components/client-list/client-list.component';
import { ClientAjoutComponent } from './components/client-ajout/client-ajout.component';
import { DetailProduitsComponent } from './components/detail-produits/detail-produits.component';
import { SidebarComponent} from './components/sidebar/sidebar.component';
import {NotfoundComponent} from './components/notfound/notfound.component';
import { FacteurClientComponent} from './components/facteur-client/facteur-client.component';
import {CategorieComponent} from './components/categorie/categorie.component';
import {ContactezNousComponent} from './components/contactez-nous/contactez-nous.component';
import {ContactnotificationComponent} from './components/contactnotification/contactnotification.component';

import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { UserComponent } from './components/user/user.component';
import { PmComponent } from './components/pm/pm.component';
import { AdminComponent } from './components/admin/admin.component';


const routes: Routes = [
  { path: 'listeproduit', component: ProduitListComponent },
  { path: '', component: AcceuilComponent },
  { path: 'listeclient', component: ClientListComponent },
  { path: 'panier', component: PanierListComponent },
  { path: 'ajoutproduit', component: ProduitAjoutComponent },
  { path: 'ajoutclient', component: ClientAjoutComponent },
  { path: 'Sidebar', component: SidebarComponent},
  { path: 'detailproduits/:reference_Produit', component: DetailProduitsComponent },
  { path: 'categorie/:id_categorie', component: CategorieComponent},
  { path: 'facture', component: FacteurClientComponent},
  {path: 'contact', component: ContactezNousComponent},
  {path: 'notificationContact', component: ContactnotificationComponent},
  { path: 'home', component: AcceuilComponent },


  // { path: 'home',component: HomeComponent  },
{ path: 'user', component: UserComponent},
{ path: 'pm', component: PmComponent},
{path: 'admin', component: AdminComponent},
{path: 'login',component: LoginComponent},
{path: 'signup',component: RegisterComponent},

{
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
},

  { path: '**', component: NotfoundComponent }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


