import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IPanier } from '../domain/ipanier';
@Injectable({
  providedIn: 'root'
})
export class PanierService {
  baseUrl = environment.myBaseUrl;
  URL = this.baseUrl + '/panier/';
  // URL = 'http://localhost:8080/panier/';
  constructor(private _http: HttpClient) { }
  getAllPanier(): Observable<IPanier[]> {
   return  this._http.get<IPanier[]>(this.URL);
  }
  AjoutPanier(panier: IPanier) {
    return this._http.post(this.URL, panier);
  }
  DeletePanier( panier_id: HTMLInputElement) {
    console.log(panier_id);
    return this._http.delete( this.URL  + panier_id);
  }
  getByID(panier_id): Observable<any> {
    // const url = this.URL + 'detailProduits' + '/' + reference_Produit;
     return this._http.get(this.URL + panier_id);
       }


}
