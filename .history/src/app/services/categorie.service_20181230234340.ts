import { Injectable } from '@angular/core';
import { IClient } from '../domain/iclient';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICategorie } from '../domain/icategorie';
// import { url } from 'inspector';

@Injectable({
  providedIn: 'root'
})
export class CategorieService {
  URL = 'http://localhost:8080/categorie/';
  constructor(private _http: HttpClient) { }
  getAllCategories(): Observable<ICategorie[]> {
   return  this._http.get<ICategorie[]>(this.URL);
  }
  // DeleteCategorie(id_categorie: number): Observable<ICategorie[]> {
  //   return  this._http.delete<ICategorie[]>(this.URL);
  //  }

  AjoutCategorie(user: ICategorie) {
    return this._http.post(this.URL, user);
  }
  DeleteCategorie( id_categorie: HTMLInputElement) {
    console.log(id_categorie);
    return this._http.delete( this.URL  + id_categorie);
  }
}
