import { Injectable } from '@angular/core';
import { IProduit } from '../domain/iProduit';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {
  baseUrl = environment.myBaseUrl;
  URL = this.baseUrl + '/client/';
  URL = 'http://localhost:8080/produit/';
  constructor(private _http: HttpClient) { }
  getAllProduit(): Observable<IProduit[]> {
   return  this._http.get<IProduit[]>(this.URL);
  }
  Ajoutproduits(user: IProduit) {
    return this._http.post(this.URL, user);
  }
  DeleteProduit( reference_Produit: HTMLInputElement) {
    console.log(reference_Produit);
    return this._http.delete( this.URL  + reference_Produit);
  }
  getByID(reference_Produit): Observable<any> {
    // const url = this.URL + 'detailProduits' + '/' + reference_Produit;
     return this._http.get(this.URL + reference_Produit);
       }
}
