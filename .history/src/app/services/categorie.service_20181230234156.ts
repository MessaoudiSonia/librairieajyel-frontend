import { Injectable } from '@angular/core';
import { IClient } from '../domain/iclient';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICategorie } from '../domain/icategorie';
// import { url } from 'inspector';

@Injectable({
  providedIn: 'root'
})
export class CategorieService {
  URL = 'http://localhost:8080/categorie/';
  constructor(private _http: HttpClient) { }
  getAllCategories(): Observable<ICategorie[]> {
   return  this._http.get<ICategorie[]>(this.URL);
  }
  // DeleteClients(id: number): Observable<IClient[]> {
  //   return  this._http.delete<IClient[]>(this.URL);
  //  }

  AjoutClients(user: IClient) {
    return this._http.post(this.URL, user);
  }
  DeleteClient( id_client: HTMLInputElement) {
    console.log(id_client);
    return this._http.delete( this.URL  + id_client);
  }
}
