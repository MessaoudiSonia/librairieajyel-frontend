import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl = environment.myBaseUrl;

  private userUrl = this.baseUrl + '/test/user';
  private pmUrl = this.baseUrl + '/test/pm';
  private adminUrl = this.baseUrl + '/test/admin';

  // private userUrl = 'http://localhost:8080/test/user';
  // private pmUrl = 'http://localhost:8080/test/pm';
  // private adminUrl = 'http://localhost:8080/test/admin';

  constructor(private http: HttpClient) { }

  getUserBoard(): Observable<string> {
    return this.http.get(this.userUrl, { responseType: 'text' });
  }

  getPMBoard(): Observable<string> {
    return this.http.get(this.pmUrl, { responseType: 'text' });
  }

  getAdminBoard(): Observable<string> {
    return this.http.get(this.adminUrl, { responseType: 'text' });
  }
}
