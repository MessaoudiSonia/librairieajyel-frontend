import { Injectable } from '@angular/core';
import { IClient } from '../domain/iclient';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IContact } from '../domain/icontact';
// import { url } from 'inspector';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  baseUrl = environment.myBaseUrl;
  URL = this.baseUrl + '/client/';
  URL = 'http://localhost:8080/contact/';
  constructor(private _http: HttpClient) { }
  getAllIContact(): Observable<IContact[]> {
   return  this._http.get<IContact[]>(this.URL);
  }
  AjoutContact(user: IContact) {
    return this._http.post(this.URL, user);
  }
  DeleteContact( id: HTMLInputElement) {
    console.log(id);
    return this._http.delete( this.URL  + id);
  }
}
