import { Injectable } from '@angular/core';
import { IClient } from '../domain/iclient';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICategorie } from '../domain/icategorie';
// import { url } from 'inspector';

@Injectable({
  providedIn: 'root'
})
export class CategorieService {
  URL = 'http://localhost:8080/categorie/';
  constructor(private _http: HttpClient) { }

  getAllCategories(): Observable<ICategorie[]> {
   return  this._http.get<ICategorie[]>(this.URL);
  }
  getByID(id_categorie): Observable<any> {
    // const url = this.URL + 'detailProduits' + '/' + reference_Produit;
     return this._http.get(this.URL + id_categorie);
     con
       }
  AjoutCategorie(categorie: ICategorie) {
    return this._http.post(this.URL, categorie);
  }
  DeleteCategorie( id_categorie: HTMLInputElement) {
    console.log(id_categorie);
    return this._http.delete( this.URL  + id_categorie);
  }
}
