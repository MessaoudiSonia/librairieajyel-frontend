import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IPanier } from '../domain/ipanier';
@Injectable({
  providedIn: 'root'
})
export class PanierService {
  URL = 'http://localhost:8080/panier/';
  constructor(private _http: HttpClient) { }
  getAllPanier(): Observable<IPanier[]> {
   return  this._http.get<IPanier[]>(this.URL);
  }


  
}
