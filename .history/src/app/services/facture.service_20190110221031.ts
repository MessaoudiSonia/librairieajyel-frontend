import { Injectable } from '@angular/core';
import { IFacture } from '../domain/ifacture';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FactureService {
  facture: IFacture;
  baseUrl = environment.myBaseUrl;
  URL = this.baseUrl + '/client/';
  // URL = 'http://localhost:8080/facture/';
  constructor(private _http: HttpClient) { }
  getAllFacture(): Observable<IFacture[]> {
   return  this._http.get<IFacture[]>(this.URL);
  }
}
