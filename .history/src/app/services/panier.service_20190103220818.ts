import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IPanier } from '../domain/ipanier';
@Injectable({
  providedIn: 'root'
})
export class PanierService {
  URL = 'http://localhost:8080/panier/';
  constructor(private _http: HttpClient) { }
  getAllPanier(): Observable<IPanier[]> {
   return  this._http.get<IPanier[]>(this.URL);
  }
  Ajoutproduits(user: IPanier) {
    return this._http.post(this.URL, user);
  }
  
  DeleteProduit( panier_id: HTMLInputElement) {
    console.log(panier_id);
    return this._http.delete( this.URL  + panier_id);
  }
  getByID(reference_Produit): Observable<any> {
    // const url = this.URL + 'detailProduits' + '/' + reference_Produit;
     return this._http.get(this.URL + reference_Produit);
       }


}
