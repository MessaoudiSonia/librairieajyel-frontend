import { Injectable } from '@angular/core';
import { IClient } from '../domain/iclient';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICategorie } from '../domain/icategorie';
import { environment } from 'src/environments/environment.prod';
// import { url } from 'inspector';

@Injectable({
  providedIn: 'root'
})
export class CategorieService {
  myBaseUrl= environment. myBaseUrl
  URL = 'http://localhost:8080/categorie/';
  constructor(private _http: HttpClient) { }

  getAllCategories(): Observable<ICategorie[]> {
   return  this._http.get<ICategorie[]>(this.URL);
  }
  getCategorieByID(id_categorie): Observable<any> {
    console.log(id_categorie);

     return this._http.get(this.URL + id_categorie);
       }
  AjoutCategorie(categorie: ICategorie) {
    return this._http.post(this.URL, categorie);
  }
  DeleteCategorie( id_categorie: HTMLInputElement) {
    console.log(id_categorie);
    return this._http.delete( this.URL  + id_categorie);
  }
}
