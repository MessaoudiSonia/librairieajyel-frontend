import { Injectable } from '@angular/core';
import {  } from '../domain/iclient';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { url } from 'inspector';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  URL = 'http://localhost:8080/client/';
  constructor(private _http: HttpClient) { }
  getAllClients(): Observable<IClient[]> {
   return  this._http.get<IClient[]>(this.URL);
  }
  // DeleteClients(id: number): Observable<IClient[]> {
  //   return  this._http.delete<IClient[]>(this.URL);
  //  }

  AjoutClients(user: IClient) {
    return this._http.post(this.URL, user);
  }
  DeleteClient( id_client: HTMLInputElement) {
    console.log(id_client);
    return this._http.delete( this.URL  + id_client);
  }
}
