import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IClient } from 'src/app/domain/iclient';
@Component({
  selector: 'app-client-ajout',
  templateUrl: './client-ajout.component.html',
  styleUrls: ['./client-ajout.component.css']
})
export class ClientAjoutComponent implements OnInit {
  
  URL = 'http://localhost:8080/client/';
  postedClient: IClient;
  constructor(private _http: HttpClient) { }

  ajoutClients(cin: HTMLInputElement, pseudo: HTMLInputElement, mdp: HTMLInputElement, prenom: HTMLInputElement,
     nom: HTMLInputElement, email: HTMLInputElement,
     tel: HTMLInputElement, adresse: HTMLInputElement) {
       this.postedClient = {cin: parseInt(cin.value, 10), pseudo: pseudo.value,
                           mdp: mdp.value, prenom: prenom.value, nom: nom.value,
                           email: email.value,
                           tel: parseInt(tel.value, 10), adresse: adresse.value};
      this._http.post(this.URL, this.postedClient).subscribe(response => console.log(response),
       error => console.log(error));
  }
  ngOnInit() {
  }

}

