import { Component, OnInit } from '@angular/core';
import { IContact } from 'src/app/domain/icontact';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-contactez-nous',
  templateUrl: './contactez-nous.component.html',
  styleUrls: ['./contactez-nous.component.scss']
})
export class ContactezNousComponent implements OnInit {

  URL = 'http://localhost:8080/contact/';
  postedContact: IContact;
  contactform = new FormGroup({
        nom : new FormControl(''),
        mail: new FormControl(''),
        tel: new FormControl(''),
        message: new FormControl('')
    });
  ngOnInit() {
  }
  ajoutContact(nom: HTMLInputElement, mail: HTMLInputElement, tel: HTMLInputElement,
    message: HTMLInputElement) {
      this.postedContact = {nom: nom.value,
                             mail: mail.value,
                             tel: +tel.value,
                             message: message.value
                            };
     this._http.post(this.URL, this.postedContact).subscribe(response => console.log(response),
      error => console.log(error));
}
constructor(private _http: HttpClient) { }
constructor(private toastrService: ToastService) {}

showSuccess() {
  this.toastrService.success('Info message');
}
    onSubmit() {
      console.warn(this.contactform.value);
    }
}
