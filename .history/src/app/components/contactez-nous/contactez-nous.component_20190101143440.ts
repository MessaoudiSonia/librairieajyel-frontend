import { Component, OnInit } from '@angular/core';
import { IContact } from 'src/app/domain/icontact';

@Component({
  selector: 'app-contactez-nous',
  templateUrl: './contactez-nous.component.html',
  styleUrls: ['./contactez-nous.component.scss']
})
export class ContactezNousComponent implements OnInit {

  constructor() { }
  URL = 'http://localhost:8080/contact/';
  postedProduit: IContact;

        nom = new FormGroup({
        mail: new FormControl(''),
        tel: new FormControl(''),
        message: new FormControl(''),
      
    });
  ngOnInit() {
  }
  ajoutContact(nom: HTMLInputElement, mail: HTMLInputElement, tel: HTMLInputElement,
    message: HTMLInputElement) {
      this.postedContact = {nom: nom.value,
                             mail: mail.value,
                             tel: +tel.value,
                             message: message.value
                            };
     this._http.post(this.URL, this.postedContact).subscribe(response => console.log(response),
      error => console.log(error));

}
