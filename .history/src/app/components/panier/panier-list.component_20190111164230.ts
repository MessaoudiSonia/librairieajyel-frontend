import { Component, OnInit } from '@angular/core';
import { IPanier } from 'src/app/domain/ipanier';
import { PanierService } from 'src/app/services/panier.service';

@Component({
  selector: 'app-panier-list',
  templateUrl: './panier-list.component.html',
  styleUrls: ['./panier-list.component.css']
})
export class PanierListComponent implements OnInit {
  info
  panier: IPanier[];
  constructor(private _service: PanierService) { }
  ngOnInit() {
    this._service.getAllPanier().subscribe(
      res => this.panier = res,
      err => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
    this.info = {
      token: this.token.getToken(),
      username: this.token.getUsername(),
      authorities: this.token.getAuthorities()
    };
  }

}
