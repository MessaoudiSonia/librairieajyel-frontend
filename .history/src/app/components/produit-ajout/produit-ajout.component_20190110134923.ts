import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl  } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { IProduit } from 'src/app/domain/iproduit';
import {ICategorie} from 'src/app/domain/icategorie';
import { CategorieComponent } from '../categorie/categorie.component';

@Component({
  selector: 'app-produit-ajout',
  templateUrl: './produit-ajout.component.html',
  styleUrls: ['./produit-ajout.component.css']
})
export class ProduitAjoutComponent implements OnInit {
  categorie: ICategorie[] ;
scat : Categorie;
 favorieI = 0;
  URL = 'http://localhost:8080/produit/';
  postedProduit: IProduit;
        produitform = new FormGroup({
        reference_Produit: new FormControl(''),
        libelle: new FormControl(''),
        prix: new FormControl(''),
        qte: new FormControl(''),
        qte_seuil: new FormControl(''),
        description: new FormControl(''),
        image: new FormControl(''),
        categorie: new FormControl(''),
    });
    ngOnInit() {
    }
    ajoutProduits(reference_Produit: HTMLInputElement, libelle: HTMLInputElement, prix: HTMLInputElement,
      qte: HTMLInputElement, qte_seuil: HTMLInputElement, description: HTMLInputElement, image: HTMLInputElement,
      categorie: HTMLInputElement) {
        this.postedProduit = {reference_Produit: reference_Produit.value,
                               libelle: libelle.value,
                               prix: +prix.value,
                               qte: +qte.value,
                               qte_seuil: +qte_seuil.value,
                               description: description.value,
                               image: image.value,
                               favorie: this.favorieI,
                               categorie: +categorie.value,
                              };
       this._http.post(this.URL, this.postedProduit).subscribe(response => console.log(response),
        error => console.log(error));
   }
    constructor(private _http: HttpClient) { }
    onSubmit() {
      console.warn(this.produitform.value);
    }
    updateProduit() {
      this.produitform.patchValue({
        image: {
          // tslint:disable-next-line:max-line-length
          street: 'https://www.citasac.fr/media/catalog/product/cache/7/image/800x800/9df78eab33525d08d6e5fb8d27136e95/t/r/trousse-ecolier-girly-rose-maped-ouvert.jpg'
        }
      });
  }
}
