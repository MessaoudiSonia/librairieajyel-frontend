import { Component, OnInit } from '@angular/core';
import { IContact } from 'src/app/domain/icontact';
import { ContactService } from 'src/app/services/contact.service';

@Component({
  selector: 'app-contactnotification',
  templateUrl: './contactnotification.component.html',
  styleUrls: ['./contactnotification.component.scss']
})
export class ContactnotificationComponent implements OnInit {
  Contact: IContact[];

  constructor(private _service: ContactService) { }

  ngOnInit() {
     this._service.getAllIContact().subscribe(
        res => this.Contact = res,
        err => console.log(`ATTENTION : Il ya l'exception : {err} `)
     );
  }

    SupprimerContact(id) {
      this._service.DeleteContact(id_client).subscribe(res => {
        console.log(res);
        console.log('suprimer');
      });
    }


//     open(content){
// this.modalService.open(content);    }
  }

