import { Component, OnInit } from '@angular/core';
import { IContact } from 'src/app/domain/icontact';

@Component({
  selector: 'app-contactnotification',
  templateUrl: './contactnotification.component.html',
  styleUrls: ['./contactnotification.component.scss']
})
export class ContactnotificationComponent implements OnInit {
  Contact : IContact[];

  constructor(private _service: ContactService) { }

  ngOnInit() {
     this._service.getAllContact().subscribe(
        res => this.Contact = res,
        err => console.log(`ATTENTION : Il ya l'exception : {err} `)
     );
  }
    // //  // tslint:disable-next-line:no-shadowed-variable
    //   SupprimerClient(IClient) {
    //    this._service.DeleteClients(IClient.id_client)
    //      .subscribe( data => {
    //        console.log(data);
    //      this.Clients.splice(this.Clients.indexOf(IClient), 1);
    // //     //  },{error}=>{
    // //     //    console.log(error);
    //     });
    // }
    SupprimerClient(id_client) {
      this._service.DeleteClient(id_client).subscribe(res => {
        console.log(res);
        console.log('suprimer');
      });
    }


//     open(content){
// this.modalService.open(content);    }
  }

