import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.scss']
})
export class CategorieComponent implements OnInit {
  searchText;
  name = 'Angular';
  produit: IProduit[];
  collection = [];
  constructor(private _service: ProduitService  ) {
    for ( let i = 1 ; i <= 100; i++) {
      this.collection.push(`Angular ${i}.0`);
     }
   }  ngOnInit() {
    this._service.getAllProduit().subscribe(
      res => this.produit = res,
      () => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
  }
}
