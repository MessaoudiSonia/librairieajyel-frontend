import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ICategorie } from 'src/app/domain/icategorie';
import { CategorieService } from 'src/app/services/categorie.service';
import { IProduit } from 'src/app/domain/iProduit';
import { ProduitService } from 'src/app/services/produit.service';

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.scss']
})
export class CategorieComponent implements OnInit {
  produit: IProduit[] ;
  cat: ICategorie[] ;
  categorieID;
  produitID;
  searchText;
  // @Input() produit: IProduit [];
  @Input() Categorie: ICategorie[];
  collection = [];
  constructor( private route: ActivatedRoute,  private produitservice: ProduitService , private categorieservice: CategorieService ) {
    for ( let i = 1 ; i <= 100; i++) {
      this.collection.push(`Angular ${i}.0`);
     }
   }
   ngOnInit() {
    this.route.params.subscribe(params => {
      this.categorieID = params['id_categorie'];
    });
    console.log(this.categorieID);

    this.categorieservice.getByID(this.categorieID).subscribe(
      res => this.catid = res,
      err => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
    this.categorieservice.getAllCategories().subscribe(
      res => this.cat = res,
      err => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
    this.produitservice.getAllProduit().subscribe(
      res => this.produit = res,
      () => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
  }
}
