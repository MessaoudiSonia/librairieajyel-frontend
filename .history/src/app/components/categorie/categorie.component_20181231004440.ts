import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ICategorie } from 'src/app/domain/icategorie';
import { CategorieService } from 'src/app/services/categorie.service';

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.scss']
})
export class CategorieComponent implements OnInit {
  cat: ICategorie[] ;
  id;
  searchText;
  @Input() categorie: ICategorie [];
  collection = [];
  constructor( private route: ActivatedRoute, private categorieservice: CategorieService ) {
    for ( let i = 1 ; i <= 100; i++) {
      this.collection.push(`Angular ${i}.0`);
     }
   }
   ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id_categorie'];
    });
    console.log(this.id);
    this.categorieservice.getByID(this.id).subscribe(
      res => this.cat = res,
      err => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
  }
}
