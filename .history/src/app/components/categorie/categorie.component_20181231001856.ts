import { Component, OnInit, Input } from '@angular/core';
import { IProduit } from 'src/app/domain/iProduit';
import { ProduitService } from 'src/app/services/produit.service';
import { ActivatedRoute } from '@angular/router';
import { ICategorie } from 'src/app/domain/icategorie';
import { CategorieService } from 'src/app/services/categorie.service';

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.scss']
})
export class CategorieComponent implements OnInit {
  p: IProduit[] ;
  id;
  searchText;
  @Input() categorie: ICategorie;
  collection = [];
  constructor( private route: ActivatedRoute, private categorieservice: CategorieService ) {
   }
   ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params[2];
    });
    console.log(this.id);
    this.categorieservice.getByID(this.id).subscribe(
      res => this.p = res,
      err => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
  }
}
