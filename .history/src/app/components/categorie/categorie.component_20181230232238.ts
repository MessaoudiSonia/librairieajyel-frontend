import { Component, OnInit, Input } from '@angular/core';
import { IProduit } from 'src/app/domain/iProduit';
import { ProduitService } from 'src/app/services/produit.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.scss']
})
export class CategorieComponent implements OnInit {
  p: IProduit[];
  id;
  searchText;
  @Input() produit: IProduit;
  collection = [];
  constructor(private _service: ProduitService , private route: ActivatedRoute, private produitservice: ProduitService ) {
    for ( let i = 1 ; i <= 100; i++) {
      this.collection.push(`Angular ${i}.0`);
     }
   }
   ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    console.log(this.produitID);
    this.produitservice.getByID(this.produitID).subscribe(
      res => this.prod = res,
      err => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
    // this._service.getAllProduit().subscribe(
    //   res => this.p = res,
    //   () => console.log(`ATTENTION : Il ya l'exception : {err} `)
    // );
  }
}
