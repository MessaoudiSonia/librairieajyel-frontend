import { Component, OnInit } from '@angular/core';
import { IProduit } from 'src/app/domain/iProduit';
import { ProduitService } from 'src/app/services/produit.service';

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.scss']
})
export class CategorieComponent implements OnInit {
  p: IProduit[];
  id;
  searchText;
  @Input() produit: IProduit;

  name = 'Angular';
  collection = [];
  constructor(private _service: ProduitService private route: ActivatedRoute,
    private produitservice: ProduitService ) {
    for ( let i = 1 ; i <= 100; i++) {
      this.collection.push(`Angular ${i}.0`);
     }
   }  ngOnInit() {
    this._service.getAllProduit().subscribe(
      res => this.produit = res,
      () => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
  }
}
