import { Component, OnInit, Input } from '@angular/core';
import { IProduit } from 'src/app/domain/iProduit';
import { ProduitService } from 'src/app/services/produit.service';
import { ActivatedRoute } from '@angular/router';
import { ICategorie } from 'src/app/domain/icategorie';

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.scss']
})
export class CategorieComponent implements OnInit {
  p: IProduit[] ;
  id;
  searchText;
  @Input() ca: ICategorie [];
  collection = [];
  constructor(private _service: ProduitService , private route: ActivatedRoute, private produitservice: ProduitService ) {
    for ( let i = 1 ; i <= 100; i++) {
      this.collection.push(`Angular ${i}.0`);
     }
   }
   ngOnInit() {
    console.log(this.id);
    this.produitservice.getByID(this.id).subscribe(
      res => this.p = res,
      err => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
  }
}
