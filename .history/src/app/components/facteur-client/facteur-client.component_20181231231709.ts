import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-facteur-client',
  templateUrl: './facteur-client.component.html',
  styleUrls: ['./facteur-client.component.scss']
})
export class FacteurClientComponent implements OnInit {

  constructor() { }
  @ViewChild('content') content: ElementRef;
  public downloadPDF()
  {
  let doc = new jsPDF();
  let specialElementHandlers = {
  'editor': function(element,renderer)
  { return true;}
  };

  let content = this.content.nativeElement;
  doc.fromHTML(content.innerHTML,15,15 {
  'width': 190,
  'elementHandlers': specialElementHandlers
  });
  doc.save('test.pdf');
  }
  ngOnInit() {
  }

}
