import { Component, OnInit } from '@angular/core';
import { IProduit } from 'src/app/domain/iProduit';
import { ProduitService } from 'src/app/services/produit.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent  implements OnInit {
  
  produit: IProduit[];
  constructor(private _service: ProduitService) { }
  ngOnInit() {
    this._service.getAllProduit().subscribe(
      res => this.produit = res,
      err => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
  }

}
