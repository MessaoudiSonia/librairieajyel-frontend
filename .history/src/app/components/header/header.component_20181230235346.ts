import { Component, OnInit } from '@angular/core';
import { IProduit } from 'src/app/domain/iProduit';
import { ProduitService } from 'src/app/services/produit.service';
import { ICategorie } from 'src/app/domain/icategorie';
import { CategorieService } from 'src/app/services/categorie.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent  implements OnInit {
  categorie : ICategorie[];
  constructor(private _service: CategorieService) { }
  ngOnInit() {
    this._service.getAllCategories().subscribe(
      res => this.categorie = res,
      err => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
  }
  // produit: IProduit[];
  // constructor(private _service: ProduitService) { }
  // ngOnInit() {
  //   this._service.getAllProduit().subscribe(
  //     res => this.produit = res,
  //     err => console.log(`ATTENTION : Il ya l'exception : {err} `)
  //   );
  // }

}
