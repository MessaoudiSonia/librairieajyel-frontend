import { Component, OnInit } from '@angular/core';
import { ICategorie } from 'src/app/domain/icategorie';
import { CategorieService } from 'src/app/services/categorie.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent  implements OnInit {
  categorie: ICategorie[];
  id;
  constructor(private _service: CategorieService) { }
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id_categorie'];
    });
  console.log(this.id);
  this.produitservice.getByID(this.id).subscribe(
    res => this.categorie = res,
    err => console.log(`ATTENTION : Il ya l'exception : {err} `)
  );
  }
  // produit: IProduit[];
  // constructor(private _service: ProduitService) { }
  // ngOnInit() {
  //   this._service.getAllProduit().subscribe(
  //     res => this.produit = res,
  //     err => console.log(`ATTENTION : Il ya l'exception : {err} `)
  //   );
  // }

}
