import { Component, OnInit } from '@angular/core';
import { ICategorie } from 'src/app/domain/icategorie';
import { CategorieService } from 'src/app/services/categorie.service';
import { IProduit } from 'src/app/domain/iProduit';
import { ProduitService } from 'src/app/services/produit.service';
import { TokenStorageService } from 'src/app/security/auth/token-storage.service';
import {NgbActiveModal, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [NgbModalConfig, NgbModal]
})
export class HeaderComponent  implements OnInit {
  categorie: ICategorie[];
  produit: IProduit[];
  info: any;

  constructor(private _servicecategorie: CategorieService, private _serviceproduit: ProduitService,
     private token: TokenStorageService, config: NgbModalConfig, private modalService: NgbModal ) { }
  ngOnInit() {
    this._servicecategorie.getAllCategories().subscribe(
      res => this.categorie = res,
      err => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
    this._serviceproduit.getAllProduit().subscribe(
         res => this.produit = res,
          err => console.log(`ATTENTION : Il ya l'exception : {err} `)
       );
  this.info = {
        token: this.token.getToken(),
        username: this.token.getUsername(),
        authorities: this.token.getAuthorities()
      };
  }
  logout() {
    this.token.signOut();
    window.location.reload();
  }
  openLogin(login) {
    this.modalService.open(login, { login: true });
  }
  openRegister(register) {
    this.modalService.open(register, { register: true });
  }
}
