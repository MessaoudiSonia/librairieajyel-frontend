import { Component, OnInit } from '@angular/core';
import { ICategorie } from 'src/app/domain/icategorie';
import { CategorieService } from 'src/app/services/categorie.service';
import { IProduit } from 'src/app/domain/iProduit';
import { ProduitService } from 'src/app/services/produit.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent  implements OnInit {
  categorie: ICategorie[];
  produit: IProduit[];
  constructor(private _servicecategorie: CategorieService, private _serviceproduit: ProduitService, private token: TokenStorageService) { }
  ngOnInit() {
    this._servicecategorie.getAllCategories().subscribe(
      res => this.categorie = res,
      err => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
    this._serviceproduit.getAllProduit().subscribe(
         res => this.produit = res,
          err => console.log(`ATTENTION : Il ya l'exception : {err} `)
       );
  }


  info: any;

  ngOnInit() {
    this.info = {
      token: this.token.getToken(),
      username: this.token.getUsername(),
      authorities: this.token.getAuthorities()
    };
  }

  logout() {
    this.token.signOut();
    window.location.reload();
  }
}
