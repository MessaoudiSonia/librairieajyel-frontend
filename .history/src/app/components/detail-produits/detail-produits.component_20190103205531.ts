import { Component, OnInit , Input} from '@angular/core';
import { IProduit } from 'src/app/domain/iProduit';
import {ActivatedRoute} from '@angular/router';
import { ProduitService } from 'src/app/services/produit.service';
import { IPanier } from 'src/app/domain/ipanier';
import { FormGroup, FormControl  } from '@angular/forms';


@Component({
  selector: 'app-detail-produits',
  templateUrl: './detail-produits.component.html',
  styleUrls: ['./detail-produits.component.css']
})
export class DetailProduitsComponent implements OnInit {
  prod: IProduit;
  produitID;
  qte;
  @Input() produit: IProduit;

  URL = 'http://localhost:8080/panier/';
  postedPanier: IPanier;
        panierform = new FormGroup({
        qte: new FormControl('')
    });

   ajoutPanier(qte) {
   return 0;
    }
    ajoutPanier(qte: HTMLInputElement ) {
        // this.postedPanier = {reference_Produit: reference_Produit.value,
                               libelle: libelle.value,
                               prix: +prix.value,
                               qte: +qte.value,
                               qte_seuil: +qte_seuil.value,
                               description: description.value,
                               image: image.value,
                               favorie: this.favorieI
                              };
       this._http.post(this.URL, this.postedProduit).subscribe(response => console.log(response),
        error => console.log(error));
   }
    constructor(private _http: HttpClient) { }
    onSubmit() {
      console.warn(this.produitform.value);
    }
    constructor(private route: ActivatedRoute,
      private produitservice: ProduitService ) { }

  ngOnInit() {
      this.route.params.subscribe(params => {
        this.produitID = params['reference_Produit'];
      });
    console.log(this.produitID);
    this.produitservice.getByID(this.produitID).subscribe(
      res => this.prod = res,
      err => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
  }


}
