import { Component, OnInit , Input} from '@angular/core';
import { IProduit } from 'src/app/domain/iProduit';
import {ActivatedRoute} from '@angular/router';
import { ProduitService } from 'src/app/services/produit.service';
import { IPanier } from 'src/app/domain/ipanier';
import { FormGroup, FormControl  } from '@angular/forms';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-detail-produits',
  templateUrl: './detail-produits.component.html',
  styleUrls: ['./detail-produits.component.css']
})
export class DetailProduitsComponent implements OnInit {
  prod: IProduit;
  produitID;
  quantite ;
  @Input() produit: IProduit;
  panier_id;
  dateP = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
  URL = 'http://localhost:8080/panier/';
  postedPanier: IPanier;
        panierform = new FormGroup({
          quantite: new FormControl('')
    });

    ajoutPanier( quantite: HTMLInputElement ) {
        this.postedPanier = {
                              //  panier_id: this.panier_id,
                               produit_id: this.produitID,
                               quantite: +quantite.value ,
                               dateP : this.dateP
                              };
      // console.log(this.panier_id);
      console.log(this.produitID);
      console.log(quantite.value);
      console.log(this.date);

       this._http.post(this.URL, this.postedPanier).subscribe(response => console.log(response),
        error => console.log(error));
   }
   constructor(private route: ActivatedRoute,
    private produitservice: ProduitService ,
    private _http: HttpClient) { }
    onSubmit() {
      console.warn(this.panierform.value);
    }
  ngOnInit() {
      this.route.params.subscribe(params => {
        this.produitID = params['reference_Produit'];
      });
    console.log(this.produitID);
    console.log(this.quantite);

    this.produitservice.getByID(this.produitID).subscribe(
      res => this.prod = res,
      err => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
  }


}
