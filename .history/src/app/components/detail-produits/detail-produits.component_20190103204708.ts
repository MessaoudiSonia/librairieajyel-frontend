import { Component, OnInit , Input} from '@angular/core';
import { IProduit } from 'src/app/domain/iProduit';
import {ActivatedRoute} from '@angular/router';
import { ProduitService } from 'src/app/services/produit.service';

@Component({
  selector: 'app-detail-produits',
  templateUrl: './detail-produits.component.html',
  styleUrls: ['./detail-produits.component.css']
})
export class DetailProduitsComponent implements OnInit {
  prod: IProduit;
  produitID;
  qte;
  @Input() produit: IProduit;
  constructor(private route: ActivatedRoute,
              private produitservice: ProduitService ) { }

   ajoutPanier(this.qte){
   return 0;
              }

  ngOnInit() {
      this.route.params.subscribe(params => {
        this.produitID = params['reference_Produit'];
      });
    console.log(this.produitID);
    this.produitservice.getByID(this.produitID).subscribe(
      res => this.prod = res,
      err => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
  }


}
