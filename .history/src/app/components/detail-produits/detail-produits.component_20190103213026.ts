import { Component, OnInit , Input} from '@angular/core';
import { IProduit } from 'src/app/domain/iProduit';
import {ActivatedRoute} from '@angular/router';
import { ProduitService } from 'src/app/services/produit.service';
import { IPanier } from 'src/app/domain/ipanier';
import { FormGroup, FormControl  } from '@angular/forms';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-detail-produits',
  templateUrl: './detail-produits.component.html',
  styleUrls: ['./detail-produits.component.css']
})
export class DetailProduitsComponent implements OnInit {
  prod: IProduit;
  produitID;
  quantite ;
  @Input() produit: IProduit;

  URL = 'http://localhost:8080/panier/';
  postedPanier: IPanier;
        panierform = new FormGroup({
          quantite: new FormControl('')
    });

    ajoutPanier(quantite: HTMLInputElement ) {
        this.postedPanier = {
          // reference_Produit: this.reference_Produit,
                               panier_id
                               
                               quantite: +quantite.value ,
                              };
       this._http.post(this.URL, this.postedPanier).subscribe(response => console.log(response),
        error => console.log(error));
   }
    onSubmit() {
      console.warn(this.panierform.value);
    }
    constructor(private route: ActivatedRoute,
      private produitservice: ProduitService ,
      private _http: HttpClient) { }

  ngOnInit() {
      this.route.params.subscribe(params => {
        this.produitID = params['reference_Produit'];
      });
    console.log(this.produitID);
    this.produitservice.getByID(this.produitID).subscribe(
      res => this.prod = res,
      err => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
  }


}
