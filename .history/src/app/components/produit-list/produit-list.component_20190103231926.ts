import { Component, OnInit } from '@angular/core';
import { IProduit } from 'src/app/domain/iProduit';
import { ProduitService } from 'src/app/services/produit.service';

@Component({
  selector: 'app-produit-list',
  templateUrl: './produit-list.component.html',
  styleUrls: ['./produit-list.component.css']
})
export class ProduitListComponent implements OnInit {
    produit: IProduit[];
    constructor(private _service: ProduitService) { }
    ngOnInit() {
      this._service.getAllProduit().subscribe(
        res => this.produit = res,
        err => console.log(`ATTENTION : Il ya l'exception : {err} `)
      );
    }
    DeleteProduit(id) {
      this._service.DeleteProduit(id).subscribe(res => {
        console.log(res);
        console.log('suprimer');
      });
    }
  }

