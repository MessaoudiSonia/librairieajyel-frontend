import { Component, OnInit } from '@angular/core';
import { ClientService } from 'src/app/services/client.service';
import { IClient } from 'src/app/domain/iclient';
@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {
  Clients: IClient[];
  constructor(private _service: ClientService) { }
  ngOnInit() {
     this._service.getAllClients().subscribe(
        res => this.Clients = res,
        err => console.log(`ATTENTION : Il ya l'exception : {err} `)
     );
  }
    // //  // tslint:disable-next-line:no-shadowed-variable
    //   SupprimerClient(IClient) {
    //    this._service.DeleteClients(IClient.id_client)
    //      .subscribe( data => {
    //        console.log(data);
    //      this.Clients.splice(this.Clients.indexOf(IClient), 1);
    // //     //  },{error}=>{
    // //     //    console.log(error);
    //     });
    // }
    SupprimerClient(id_client) {
      this._service.DeleteClient(id_client).subscribe(res => {
        // console.log(res);
        // console.log('suprimer');
      });
    this  this.Clients.filter(e => e.id_client !== id_client);

    }


//     open(content){
// this.modalService.open(content);    }
  }

