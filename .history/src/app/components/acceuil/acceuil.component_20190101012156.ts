import { Component, OnInit, Input } from '@angular/core';
import { IProduit } from 'src/app/domain/iProduit';
import { ProduitService } from 'src/app/services/produit.service';


@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.css']
})
export class AcceuilComponent implements OnInit {
  searchText;
  name = 'Angular';
  produit: IProduit[];
  collection = [];
  produit.favorie = 0;
  // @Input() favorie = 0;
  constructor(private _service: ProduitService  ) {
    for ( let i = 1 ; i <= 100; i++) {
      this.collection.push(`Angular ${i}.0`);
     }
   }
   likeClick() {
     this.favorie++;
     console.log(this.favorie);
   }
   dislikeClick() {
    this.favorie--;
  }

   ngOnInit() {
    this._service.getAllProduit().subscribe(
      res => this.produit = res,
      () => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
  }
}
