import { Component, OnInit } from '@angular/core';
import { IProduit } from 'src/app/domain/iProduit';
import { ProduitService } from 'src/app/services/produit.service';
import { MypipePipe } from 'src/app/mypipe.pipe';


@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.css']
})
export class AcceuilComponent implements OnInit {
  searchText;
  name = 'Angular';
  produit: IProduit[];
  collection = [];
  constructor(private _service: ProduitService  ) {
    for ( let i = 1 ; i <= 100; i++) {
      this.collection.push(`Angular ${i}.0`);
     }
   }  ngOnInit() {
    this._service.getAllProduit().subscribe(
      res => this.produit = res,
      () => console.log(`ATTENTION : Il ya l'exception : {err} `)
    );
  }
}
