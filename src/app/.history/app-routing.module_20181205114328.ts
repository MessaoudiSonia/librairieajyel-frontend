import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcceuilComponent } from './components/acceuil/acceuil.component';
import { ProduitListComponent } from './components/produit-list/produit-list.component';
import { PanierListComponent } from './components/panier-list/panier-list.component';
import { ProduitAjoutComponent } from './components/produit-ajout/produit-ajout.component';
import { FactureListComponent } from './components/facture-list/facture-list.component';
import { ClientListComponent } from './components/client-list/client-list.component';
import { ClientAjoutComponent } from './components/client-ajout/client-ajout.component';


const routes: Routes = [
  { path: 'listeproduit', component: ProduitListComponent },
  { path: '', component: AcceuilComponent },
  { path: 'listeclient', component: ClientListComponent },
  { path: 'listepanier', component: PanierListComponent },
  { path: 'ajoutproduit', component: ProduitAjoutComponent },
  { path: 'ajoutclient', component: ClientAjoutComponent },
  { path: 'listefacture', component: FactureListComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


