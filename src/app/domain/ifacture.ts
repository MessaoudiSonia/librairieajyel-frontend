export interface IFacture {
  reference: string;
  date: string;
  montant: number;
}
