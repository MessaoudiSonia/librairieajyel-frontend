import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactnotificationComponent } from './contactnotification.component';

describe('ContactnotificationComponent', () => {
  let component: ContactnotificationComponent;
  let fixture: ComponentFixture<ContactnotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactnotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactnotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
