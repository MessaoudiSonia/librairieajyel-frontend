import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacteurClientComponent } from './facteur-client.component';

describe('FacteurClientComponent', () => {
  let component: FacteurClientComponent;
  let fixture: ComponentFixture<FacteurClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacteurClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacteurClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
